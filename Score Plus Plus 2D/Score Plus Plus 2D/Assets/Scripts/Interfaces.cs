﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITileInterface{
	int GetTileCode();
	void SetTileCode(int tileCode);
}

